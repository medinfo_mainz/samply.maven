# Samply Maven

## How to use the internal IMBEI maven repository

Maven uses several repositories:

1. $HOME/.m2/repository
2. the active repositories in your settings.xml and pom.xml
3. the central Maven Repository

If maven searches for artifacts this list of repository is searched in this
particular order. The first artifact that matches the requirements is used.

The IMBEI hosts a Sonatype Nexus Maven Repository on

```
https://maven.imbei.uni-mainz.de:8443
```

This server is only accesible on the `134.93.0.0/16` network. If you want to
access it from home, you must connect to the Uni-Mainz VPN first. For users without access
to the Uni Mainz VPN see [this page](https://maven.imbei.uni-mainz.de/external.html) for instructions.

```xml

<?xml version="1.0" encoding="UTF-8"?>
<settings
    xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.1.0 http://maven.apache.org/xsd/settings-1.1.0.xsd"
    xmlns="http://maven.apache.org/SETTINGS/1.1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

    <profiles>
        <profile>
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
            <repositories>
                <repository>
                    <releases>
                        <enabled>false</enabled>
                    </releases>
                    <snapshots>
                        <enabled>true</enabled>
                    </snapshots>
                    <id>oss-snapshots</id>
                    <url>https://maven.imbei.uni-mainz.de:8443/content/repositories/oss-snapshots</url>
                </repository>
                <repository>
                    <releases>
                        <enabled>false</enabled>
                    </releases>
                    <snapshots>
                        <enabled>true</enabled>
                    </snapshots>
                    <id>snapshots</id>
                    <url>https://maven.imbei.uni-mainz.de:8443/content/repositories/snapshots</url>
                </repository>

                <repository>
                    <releases>
                        <enabled>true</enabled>
                    </releases>
                    <snapshots>
                        <enabled>false</enabled>
                    </snapshots>
                    <id>oss-releases</id>
                    <url>https://maven.imbei.uni-mainz.de:8443/content/repositories/oss-releases</url>
                </repository>
                <repository>
                    <releases>
                        <enabled>true</enabled>
                    </releases>
                    <snapshots>
                        <enabled>false</enabled>
                    </snapshots>
                    <id>releases</id>
                    <url>https://maven.imbei.uni-mainz.de:8443/content/repositories/releases</url>
                </repository>
            </repositories>
        </profile>
    </profiles>
</settings>
```

If you want to upload your artifacts to that maven repository, create a new
"encrypted" master password with maven: `mvn --encrypt-master-password
$SECURE_PASSWORD` (The newest maven version asks for your password, the old
versions require you to enter it as an argument). You do not need to remember
this password. Use the output it generates to create the file
`$HOME/.m2/settings-security.xml`:

```xml
<settingsSecurity>
    <master>$OUTPUT</master>
</settingsSecurity>
```

Now you can encrypt other passwords (e.g. the password for your Nexus account /
Active Directory account) with `mvn --encrypt-password $AD_PASSWORD` and use
those encrypted passwords in your settings.xml (replace pweingar with your AD
account name):

```xml
<settings ...>

    <servers>
        <server>
            <id>releases</id>
            <username>pweingar</username>
            <password>$ENCRYPTED</password>
        </server>
        <server>
            <id>snapshots</id>
            <username>pweingar</username>
            <password>$ENCRYPTED</password>
        </server>
        <server>
            <id>imbei-maven</id>
            <username>pweingar</username>
            <password>$ENCRYPTED</password>
        </server>
    </servers>

    <profiles>
      ....
    </profiles>
</settings>
```

Now you have configured maven to use authenication if required but your projects
need to know where to deploy the artifacts. You can do this by either adding the
following tags in your pom.xml (for closed source projects. For open source projects use `oss-releases` and `oss-snapshots`.):

```xml
  <distributionManagement>
    <repository>
      <id>imbei-maven</id>
      <url>https://maven.imbei.uni-mainz.de:8443/content/repositories/releases</url>
    </repository>
    <snapshotRepository>
      <id>imbei-maven</id>
      <url>https://maven.imbei.uni-mainz.de:8443/content/repositories/snapshots</url>
    </snapshotRepository>
  </distributionManagement>
```

Another way is to use this `pom.xml` as a parent, inheriting all dependency
versions, but keep in mind that using this parent pom.xml may have sideeffects:

```xml
<parent>
    <groupId>de.samply</groupId>
    <artifactId>parent</artifactId>
    <version>${VERSION}</version>
</parent>
```
