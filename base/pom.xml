<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <groupId>de.samply</groupId>
    <artifactId>parent</artifactId>
    <version>8.0.0-SNAPSHOT</version>
    <name>The root maven POM artifact for Samply Projects</name>
    <packaging>pom</packaging>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>

        <version.mojarra>2.2.10</version.mojarra>
        <version.myfaces>2.2.6</version.myfaces>
        <version.httpclient>4.5.1</version.httpclient>

        <version.jersey>2.13</version.jersey>
        <version.postgres>9.3-1102-jdbc41</version.postgres>

        <version.javax.servlet>3.0.1</version.javax.servlet>
        <version.javax.validation>1.1.0.Final</version.javax.validation>
        <version.javax.mail>1.4.7</version.javax.mail>

        <version.commons.codec>1.9</version.commons.codec>
        <version.nimbus.jwt>4.4</version.nimbus.jwt>
        <version.gson>2.3</version.gson>
        <version.jettison>1.3.7</version.jettison>

        <version.guava>18.0</version.guava>
        <version.jaxb>2.2.12</version.jaxb>

        <version.maven.plugin.annotations>3.4</version.maven.plugin.annotations>
        <version.maven.plugin.api>3.3.3</version.maven.plugin.api>

        <version.log4j2>2.1</version.log4j2>
        <version.junit>4.11</version.junit>
    </properties>

    <organization>
        <name>Universitätsmedizin Mainz, Abteilung IMBEI</name>
    </organization>

    <dependencyManagement>
        <dependencies>
            <!-- Database Drivers -->

            <dependency>
                <groupId>org.postgresql</groupId>
                <artifactId>postgresql</artifactId>
                <version>${version.postgres}</version>
                <scope>runtime</scope>
            </dependency>



            <!-- JSF libraries -->

            <dependency>
                <groupId>javax.servlet</groupId>
                <artifactId>javax.servlet-api</artifactId>
                <version>${version.javax.servlet}</version>
            </dependency>

            <dependency>
                <groupId>org.apache.myfaces.core</groupId>
                <artifactId>myfaces-impl</artifactId>
                <version>${version.myfaces}</version>
            </dependency>

            <dependency>
                <groupId>com.sun.faces</groupId>
                <artifactId>jsf-api</artifactId>
                <version>${version.mojarra}</version>
            </dependency>

            <dependency>
                <groupId>com.sun.faces</groupId>
                <artifactId>jsf-impl</artifactId>
                <version>${version.mojarra}</version>
            </dependency>

            <dependency>
                <groupId>javax.validation</groupId>
                <artifactId>validation-api</artifactId>
                <version>${version.javax.validation}</version>
            </dependency>



            <!-- Most common Apache Libraries -->

            <dependency>
                <groupId>org.apache.httpcomponents</groupId>
                <artifactId>httpclient</artifactId>
                <version>${version.httpclient}</version>
            </dependency>

            <dependency>
                <groupId>org.apache.logging.log4j</groupId>
                <artifactId>log4j-core</artifactId>
                <version>${version.log4j2}</version>
            </dependency>

            <dependency>
                <groupId>org.apache.logging.log4j</groupId>
                <artifactId>log4j-web</artifactId>
                <version>${version.log4j2}</version>
            </dependency>

            <dependency>
                <groupId>commons-codec</groupId>
                <artifactId>commons-codec</artifactId>
                <version>${version.commons.codec}</version>
            </dependency>



            <!-- Jersey Libraries -->

            <dependency>
                <groupId>org.glassfish.jersey.core</groupId>
                <artifactId>jersey-client</artifactId>
                <version>${version.jersey}</version>
            </dependency>

            <dependency>
                <groupId>org.glassfish.jersey.containers</groupId>
                <artifactId>jersey-container-servlet</artifactId>
                <version>${version.jersey}</version>
            </dependency>

            <dependency>
                <groupId>org.glassfish.jersey.media</groupId>
                <artifactId>jersey-media-json-jackson</artifactId>
                <version>${version.jersey}</version>
            </dependency>



            <!-- JWT Libraries -->

            <dependency>
                <groupId>com.nimbusds</groupId>
                <artifactId>nimbus-jose-jwt</artifactId>
                <version>${version.nimbus.jwt}</version>
            </dependency>



            <!-- Maven Plugin Development -->

            <dependency>
                <groupId>org.apache.maven</groupId>
                <artifactId>maven-plugin-api</artifactId>
                <version>${version.maven.plugin.api}</version>
            </dependency>

            <dependency>
                <groupId>org.apache.maven.plugin-tools</groupId>
                <artifactId>maven-plugin-annotations</artifactId>
                <version>${version.maven.plugin.annotations}</version>
                <scope>provided</scope>
            </dependency>



            <!-- Misc -->

            <dependency>
                <groupId>com.google.code.gson</groupId>
                <artifactId>gson</artifactId>
                <version>${version.gson}</version>
            </dependency>

            <dependency>
                <groupId>org.codehaus.jettison</groupId>
                <artifactId>jettison</artifactId>
                <version>${version.jettison}</version>
            </dependency>

            <dependency>
                <groupId>javax.mail</groupId>
                <artifactId>mail</artifactId>
                <version>${version.javax.mail}</version>
            </dependency>

            <dependency>
                <groupId>com.google.guava</groupId>
                <artifactId>guava</artifactId>
                <version>${version.guava}</version>
            </dependency>

            <dependency>
                <groupId>javax.xml.bind</groupId>
                <artifactId>jaxb-api</artifactId>
                <version>${version.jaxb}</version>
            </dependency>



            <!-- Unit Tests -->

            <dependency>
                <groupId>junit</groupId>
                <artifactId>junit</artifactId>
                <version>${version.junit}</version>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-war-plugin</artifactId>
                <version>2.6</version>
                <configuration>
                    <archive>
                        <manifest>
                            <addDefaultImplementationEntries>true</addDefaultImplementationEntries>
                            <addDefaultSpecificationEntries>true</addDefaultSpecificationEntries>
                        </manifest>
                        <manifestEntries>
                            <Build-Timestamp>${maven.build.timestamp}</Build-Timestamp>
                        </manifestEntries>
                    </archive>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-jar-plugin</artifactId>
                <version>2.5</version>
                <configuration>
                    <archive>
                        <manifest>
                            <addDefaultImplementationEntries>true</addDefaultImplementationEntries>
                            <addDefaultSpecificationEntries>true</addDefaultSpecificationEntries>
                        </manifest>
                        <manifestEntries>
                            <Build-Timestamp>${maven.build.timestamp}</Build-Timestamp>
                        </manifestEntries>
                    </archive>
                </configuration>
            </plugin>

            <plugin>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.2</version>
                <configuration>
                    <source>1.7</source>
                    <target>1.7</target>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-release-plugin</artifactId>
                <version>2.5</version>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>2.17</version>
                <configuration>
                    <skipTests>true</skipTests>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-source-plugin</artifactId>
                <version>2.3</version>
                <executions>
                    <execution>
                        <id>attach-sources</id>
                        <goals>
                            <goal>jar</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-eclipse-plugin</artifactId>
                <version>2.9</version>
                <configuration>
                    <downloadSources>true</downloadSources>
                    <downloadJavadocs>true</downloadJavadocs>
                </configuration>
            </plugin>

            <plugin>
                <artifactId>maven-site-plugin</artifactId>
                <version>3.4</version>
                <dependencies>
                    <dependency>
                        <groupId>org.apache.maven.wagon</groupId>
                        <artifactId>wagon-webdav-jackrabbit</artifactId>
                        <version>2.6</version>
                    </dependency>
                </dependencies>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <version>2.10.3</version>
            </plugin>

            <plugin>
                <groupId>com.mycila</groupId>
                <artifactId>license-maven-plugin</artifactId>
                <version>2.11</version>
                <configuration>
                    <header>de/samply/licenses/AGPL-3.txt</header>
                    <excludes>
                        <exclude>src/site/**</exclude>
                        <exclude>pom.xml</exclude>
                        <exclude>**/*.episode</exclude>
                        <exclude>**/*.svg</exclude>
                    </excludes>
                </configuration>
                <dependencies>
                    <dependency>
                        <groupId>de.samply</groupId>
                        <artifactId>licenses</artifactId>
                        <version>1.0.0</version>
                    </dependency>
                </dependencies>
            </plugin>
        </plugins>
    </build>

    <reporting>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-project-info-reports-plugin</artifactId>
                <version>2.8</version>
                <reportSets>
                    <reportSet>
                        <reports>
                        </reports>
                    </reportSet>
                </reportSets>
                <configuration>
                    <dependencyLocationEnabled>false</dependencyLocationEnabled>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <version>2.10.3</version>
            </plugin>
        </plugins>
    </reporting>

    <dependencies>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <distributionManagement>
        <repository>
            <id>imbei-maven</id>
            <url>https://maven.imbei.uni-mainz.de:8443/content/repositories/oss-releases</url>
        </repository>
        <snapshotRepository>
            <id>imbei-maven</id>
            <url>https://maven.imbei.uni-mainz.de:8443/content/repositories/oss-snapshots</url>
        </snapshotRepository>
        <site>
            <id>imbei-maven</id>
            <url>dav:https://maven.imbei.uni-mainz.de:8443/content/sites/Sites</url>
        </site>
    </distributionManagement>

    
    <profiles>
        <profile>
            <id>all</id>
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
            <modules>
                <module>../servlet/</module>
                <module>../jsf/</module>
                <module>../plugin/</module>
            </modules>
        </profile>
        <profile>
            <id>doc</id>
            <modules>
            </modules>
        </profile>
    </profiles>

</project>
